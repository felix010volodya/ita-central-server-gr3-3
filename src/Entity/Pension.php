<?php

namespace App\Entity;

use App\Model\Client\ClientHandler;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Clent\ClentInterface;

/**
 * @ORM\Entity
 */
class Pension extends BookingObject
{
    /**
     * @ORM\Column(type="boolean", length=128)
     */
    private $cook;

    /**
     * @param mixed $cook
     * @return Pension
     */
    public function setCook($cook)
    {
        $this->cook = $cook;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCook()
    {
        return $this->cook;
    }
}
